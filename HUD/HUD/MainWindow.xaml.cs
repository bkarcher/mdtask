﻿using Microsoft.UI.Xaml;

namespace HUD
{
    public sealed partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();
            this.Title = "HUD";
        }
    }
}
