﻿using HUD.Models;
using System.Collections.Generic;
using System;
using System.Linq;
using Windows.Storage;
using Windows.Storage.AccessCache;
using System.Threading.Tasks;

namespace HUD.Services
{
    class FileStorage
    {
        public void StorePath(StorageFolder folder)
        {
            StorageApplicationPermissions.MostRecentlyUsedList.Add(folder, DateTime.Now.ToString());
        }

        public async Task<StorageFolder> GetStoredPath()
        {
            if (!StorageApplicationPermissions.MostRecentlyUsedList.Entries.Any())
                return null;

            var e = StorageApplicationPermissions.MostRecentlyUsedList.Entries.OrderByDescending(entry => DateTime.Parse(entry.Metadata)).FirstOrDefault();

            return await StorageApplicationPermissions.MostRecentlyUsedList.GetFolderAsync(e.Token);
        }

        public async Task<DataModel> Load(StorageFolder folder)
        {
            return new DataModel()
            {
                TodoBoard = await LoadBoard(folder),
                Journal = await LoadJournal(folder),
                Reminders = await LoadReminders(folder),
                MdFiles = await LoadMdFiles(folder),
                Image = (await folder.GetFilesAsync()).Where(f => f.Name.EndsWith(".jpg")).FirstOrDefault()?.Path
        };
        }

        public async Task Save(StorageFolder folder, DataModel model)
        {
            await this.SaveBoard(folder, model.TodoBoard);
            await this.SaveJournal(folder, model.Journal);
            await this.SaveMdFiles(folder, model.MdFiles);
            await this.SaveReminders(folder, model.Reminders);
        }

        private async Task<TodoBoard> LoadBoard(StorageFolder folder)
        {
            string data = await FileIO.ReadTextAsync(await folder.GetFileAsync("todo.yaml"));

            var deserializer = new YamlDotNet.Serialization.Deserializer();

            object tree = deserializer.Deserialize<object>(data);

            return BoardFromTree(tree);
        }

        private async Task SaveBoard(StorageFolder folder, TodoBoard board)
        {
            var serializer = new YamlDotNet.Serialization.Serializer();

            string data = serializer.Serialize(BoardToTree(board));

            await FileIO.WriteTextAsync(await folder.GetFileAsync("todo.yaml"), data);
        }

        private async Task SaveJournal(StorageFolder folder, Journal journal)
        {
            var serializer = new YamlDotNet.Serialization.Serializer();

            string data = serializer.Serialize(JournalToTree(journal));

            await FileIO.WriteTextAsync(await folder.GetFileAsync("journal.yaml"), data);
        }

        private TodoBoard BoardFromTree(object tree)
        {
            TodoBoard board = new TodoBoard();

            if (tree is Dictionary<object, object>)
            {
                foreach (KeyValuePair<object, object> todoList in (Dictionary<object, object>)tree)
                {
                    if(todoList.Key is string)
                    {
                        board.Add(ListFromTree((string)todoList.Key, todoList.Value));
                    }
                }
            }

            return board;
        }

        private TodoList ListFromTree(string Title, object tree)
        {
            TodoList list = new TodoList() { Title = Title };

            if(tree is List<object>)
            {
                foreach(object todo in (List<object>)tree)
                {
                    list.Todos.Add(TodoFromTree(todo));
                }
            }

            return list;
        }

        private Todo TodoFromTree(object tree)
        {
            Todo todo = new Todo();

            if(tree is string)
            {
                return new Todo((string)tree);
            }
            else if(tree is Dictionary<object, object>)
            {
                Dictionary<object, object> todoTree = (Dictionary<object, object>)tree;

                todo.Title = (string)todoTree.GetValueOrDefault("title");

                if(todoTree.ContainsKey("date"))
                    todo.Date = DateTime.Parse((string)todoTree.GetValueOrDefault("date"));

                if(todoTree.GetValueOrDefault("info") is string)
                {
                    todo.Info.Add((string)todoTree.GetValueOrDefault("info"));
                }
                else if(todoTree.GetValueOrDefault("info") is List<object>)
                {
                    foreach(object info in (List <object>)todoTree.GetValueOrDefault("info"))
                    {
                        todo.Info.Add((string)info);
                    }
                }
            }

            return todo;
        }

        private Dictionary<object, object> BoardToTree(TodoBoard board)
        {
            Dictionary<object, object> tree = new Dictionary<object, object>();

            board.ForEach(l => tree.Add(l.Title, ListToTree(l)));

            return tree;
        }

        private List<object> ListToTree(TodoList list)
        {
            return list.Todos.Select(t => TodoToTree(t)).ToList();
        }

        private object TodoToTree(Todo todo)
        {
             if (!todo.Date.HasValue && todo.Info.Count == 0)
                return todo.Title;

            Dictionary<object, object> tree = new Dictionary<object, object>();

            tree.Add("title", todo.Title);

            if (todo.Date.HasValue)
                tree.Add("date", todo.Date.Value.ToString("yyyy-MM-dd"));

            if (todo.Info.Count == 1)
                tree.Add("info", todo.Info.First());
            else if (todo.Info.Count > 1)
                tree.Add("info", todo.Info);

            return tree;
        }

        private async Task<Journal> LoadJournal(StorageFolder folder)
        {
            string data = await FileIO.ReadTextAsync(await folder.GetFileAsync("journal.yaml"));

            var deserializer = new YamlDotNet.Serialization.Deserializer();

            object tree = deserializer.Deserialize<object>(data);

            return JournalFromTree(tree);
        }

        private Journal JournalFromTree(object tree)
        {
            Journal data = new Journal();

            if (tree is Dictionary<object, object>)
            {
                foreach (KeyValuePair<object, object> entry in (Dictionary<object, object>)tree)
                {
                    data.Add(new JournalEntry(DateTimeOffset.Parse((string)entry.Key), (string)entry.Value));
                }
            }

            return data;
        }

        private object JournalToTree(Journal journal)
        {
            var tree = new Dictionary<object, object>();

            journal.ForEach(e => tree.Add(e.Date.ToString("yyyy-MM-dd"), e.Content));

            return tree;
        }

        private async Task<Reminders> LoadReminders(StorageFolder folder)
        {
            string data = await FileIO.ReadTextAsync(await folder.GetFileAsync("reminders.yaml"));

            var deserializer = new YamlDotNet.Serialization.Deserializer();

            object tree = deserializer.Deserialize<object>(data);

            return RemindersFromTree(tree);
        }

        private Reminders RemindersFromTree(object tree)
        {
            Reminders data = new Reminders();

            if(tree is Dictionary<object, object>)
            {
                foreach (KeyValuePair<object, object> entry in (Dictionary<object, object>)tree)
                {
                    if (entry.Value is string)
                        data.Add(new Reminder((string)entry.Key, new List<string>() { (string)entry.Value }));
                    else if (entry.Value is List<object>)
                        data.Add(new Reminder((string)entry.Key, ((List<object>)entry.Value).Select(e => (string)e).ToList()));
                }
            }

            return data;
        }

        private object RemindersToTree(Reminders reminders)
        {
            var tree = new Dictionary<object, object>();

            reminders.ForEach(r =>
            {
                if (r.Content.Count == 1)
                    tree.Add(r.Title, r.Content[0]);
                else if (r.Content.Count > 1)
                {
                    List<object> items = new List<object>();

                    r.Content.ForEach(c => items.Add(c));

                    tree.Add(r.Title, items);
                }
            });

            return tree;
        }

        private async Task SaveReminders(StorageFolder folder, Reminders reminders)
        {
            var serializer = new YamlDotNet.Serialization.Serializer();

            string data = serializer.Serialize(RemindersToTree(reminders));

            await FileIO.WriteTextAsync(await folder.GetFileAsync("reminders.yaml"), data);
        }

        private async Task<MdFiles> LoadMdFiles(StorageFolder folder)
        {
            IEnumerable<StorageFile> files = (await folder.GetFilesAsync()).Where(f => f.Name.EndsWith(".md"));

            MdFiles data = new MdFiles(); 

            foreach(StorageFile f in files)
            {
                data.Add(new MdFile(f.Name, await FileIO.ReadTextAsync(f)));
            }

            return data;
        }

        private async Task SaveMdFiles(StorageFolder folder, MdFiles files)
        {
            foreach(MdFile f in files)
                await FileIO.WriteTextAsync(await folder.GetFileAsync(f.Title), f.Content);
        }
    }
}
