﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace HUD.Models
{
    class ObservableChangeNotifyingCollection<T> : ObservableCollection<T>, IUnsavedNotifier where T : IUnsavedNotifier
    {
        public event NewUnsavedChangeEventHandler NewUnsavedChange;

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                RegisterPropertyChanged(e.NewItems);
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                UnRegisterPropertyChanged(e.OldItems);
            }
            else if (e.Action == NotifyCollectionChangedAction.Replace)
            {
                UnRegisterPropertyChanged(e.OldItems);
                RegisterPropertyChanged(e.NewItems);
            }

            this.NewUnsavedChange?.Invoke();

            base.OnCollectionChanged(e);
        }

        protected override void ClearItems()
        {
            UnRegisterPropertyChanged(this);
            base.ClearItems();
        }

        private void RegisterPropertyChanged(IList items)
        {
            foreach (IUnsavedNotifier item in items)
            {
                if (item != null)
                {
                    item.NewUnsavedChange += this.PropagateChange;
                }
            }
        }

        private void UnRegisterPropertyChanged(IList items)
        {
            foreach (IUnsavedNotifier item in items)
            {
                if (item != null)
                {
                    item.NewUnsavedChange -= this.PropagateChange;
                }
            }
        }

        private void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //launch an event Reset with name of property changed
            base.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        private void PropagateChange() => this.NewUnsavedChange?.Invoke();
    }
}
