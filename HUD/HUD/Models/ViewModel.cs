﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace HUD.Models
{
    class ViewModel : ChangeNotifier
    {
        public TodoBoardViewModel TodoBoard { get; set; }
        public JournalViewModel Journal { get; set; }
        public RemindersViewModel Reminders { get; set; }
        public MdFilesViewModel MdFiles { get; set; }
        public ImageViewModel Image { get; set; }

        public bool LeftPaneOpen
        { 
            get { return lpo; }
            set { this.SetField(ref lpo, value); }
        }

        public bool RightPaneOpen
        {
            get { return rpo; }
            set { this.SetField(ref rpo, value); }

        }
        public bool Unsaved
        {
            get { return u; }
            set { this.SetField(ref u, value); }
        }

        private bool lpo;
        private bool rpo;
        private bool u;

        public ViewModel()
        {
            LeftPaneOpen = false;
            RightPaneOpen = false;
            Unsaved = false;

            TodoBoard = new TodoBoardViewModel();
            Journal = new JournalViewModel();
            Reminders = new RemindersViewModel();
            MdFiles = new MdFilesViewModel();
            Image = new ImageViewModel();

            TodoBoard.NewUnsavedChange += () => { this.Unsaved = true; };
            Journal.NewUnsavedChange += () => { this.Unsaved = true; };
            Reminders.NewUnsavedChange += () => { this.Unsaved = true; };
            MdFiles.NewUnsavedChange += () => { this.Unsaved = true; };
        }

        public void Merge(DataModel incoming)
        {
            this.TodoBoard.Clear();

            foreach (TodoList l in incoming.TodoBoard)
                this.TodoBoard.Add(new TodoListViewModel(l));

            this.Journal.Clear();

            foreach (JournalEntry e in incoming.Journal)
                this.Journal.Add(new JournalEntryViewModel(e));

            this.Reminders.Clear();

            foreach (Reminder r in incoming.Reminders)
                this.Reminders.Add(new ReminderViewModel(r, this.Reminders));

            this.MdFiles.Clear();

            foreach (MdFile f in incoming.MdFiles)
                this.MdFiles.Add(new MdFileViewModel(f));

            this.Image.Image = incoming.Image ?? ImageViewModel.placeholder;
            this.Unsaved = false;
        }
        
        public DataModel ToModel()
        {
            return new DataModel()
            {
                TodoBoard = new TodoBoard(this.TodoBoard.Select(tl => new TodoList
                {
                    Title = tl.Title,
                    Todos = tl.Todos.Select(t => new Todo
                    {
                        Title = t.Title,
                        Date = t.HasDate ? (DateTimeOffset?)t.Date : null,
                        Info = t.Info.Select(i => i.Data).ToList()
                    }).ToList()
                })),
                Journal = new Journal(this.Journal.Select(e => new JournalEntry(e.Date, e.Content))),
                Reminders = new Reminders(this.Reminders.Select(r => new Reminder(r.Title, r.Items.Select(i => i.Data).ToList()))),
                MdFiles = new MdFiles(this.MdFiles.Select(f => new MdFile(f.Title, f.Content)))
            };
        }
    }

    class TodoBoardViewModel : ObservableChangeNotifyingCollection<TodoListViewModel>
    {
        public TodoBoardViewModel() { }

        public TodoBoardViewModel(TodoBoard board)
        {
            board.ForEach(l => this.Add(new TodoListViewModel(l)));
        }
    }

    class TodoListViewModel : ChangeNotifier
    {
        public string Title
        {
            get { return t; }
            set { this.SetChangeField(ref t, value); }
        }

        public ObservableChangeNotifyingCollection<TodoViewModel> Todos { get; set; }

        private string t;

        public TodoListViewModel(TodoList l)
        {
            Title = l.Title;
            Todos = new ObservableChangeNotifyingCollection<TodoViewModel>();
            l.Todos.ForEach(t => this.Todos.Add(new TodoViewModel(t, this)));
            
            this.Todos.NewUnsavedChange += () => { this.NewChange(); };
        }
    }

    class TodoViewModel : ChangeNotifier
    {
        public string Title
        {
            get { return t; }
            set { this.SetChangeField(ref t, value); }
        }

        public ObservableChangeNotifyingCollection<InfoViewModel> Info { get; set; }
        public DateTimeOffset Date
        {
            get { return d; }
            set { this.SetField(ref d, value); }
        }

        public bool HasDate
        {
            get { return hd; }
            set { this.SetChangeField(ref hd, value); }
        }

        public bool EditMode
        {
            get { return edit; }
            set { this.SetField(ref edit, value); }
        }

        private TodoListViewModel parent;

        private string t;
        private DateTimeOffset d;
        private bool hd;
        private bool edit;

        public TodoViewModel(Todo t, TodoListViewModel parent)
        {
            edit = false;
            Title = t.Title;
            Date = t.Date ?? new DateTimeOffset(DateTime.Now);
            HasDate = t.Date.HasValue;
            Info = new ObservableChangeNotifyingCollection<InfoViewModel>();
            this.parent = parent;

            foreach (string i in t.Info)
                this.Info.Add(new InfoViewModel(i, this));

            Info.NewUnsavedChange += () => { this.NewChange(); };
        }

        public void DeleteSelf()
        {
            this.NewChange();
            this.parent.Todos.Remove(this);
        }
    }

    class InfoViewModel : ChangeNotifier
    {
        public string Data
        {
            get { return d; }
            set { this.SetChangeField(ref d, value); }
        }

        private string d;

        private TodoViewModel parent;

        public InfoViewModel(string i, TodoViewModel parent)
        {
            this.Data = i;
            this.parent = parent;
        }
        public void DeleteSelf()
        {
            this.NewChange();
            this.parent.Info.Remove(this);
        }
    }

    class JournalViewModel : ObservableChangeNotifyingCollection<JournalEntryViewModel>
    {
        public List<JournalEntryViewModel> PastEntries { get { return this.Where(e => !IsThisWeek(e.Date)).ToList(); } }
        public List<JournalEntryViewModel> ThisWeek { get { return this.Where(e => IsThisWeek(e.Date)).ToList(); } }

        public JournalViewModel() : base() { }

        private bool IsThisWeek(DateTimeOffset d)
        {
            var cal = System.Globalization.DateTimeFormatInfo.CurrentInfo.Calendar;
            var d1 = d.Date.AddDays(-1 * (int)cal.GetDayOfWeek(d.Date));
            var d2 = DateTime.Now.Date.AddDays(-1 * (int)cal.GetDayOfWeek(DateTime.Now));

            return d1 == d2;
        }
    }

    class JournalEntryViewModel : ChangeNotifier
    {
        public DateTimeOffset Date
        {
            get { return d; }
            set { this.SetField(ref d, value); }
        }

        public string Content
        {
            get { return c; }
            set { this.SetChangeField(ref c, value); }
        }

        public bool EditMode
        {
            get { return edit; }
            set { this.SetField(ref edit, value); }
        }

        private DateTimeOffset d;
        private string c;
        private bool edit;

        public JournalEntryViewModel(JournalEntry e)
        {
            this.Date = e.Date;
            this.Content = e.Content;
            this.EditMode = false;
        }
    }

    class RemindersViewModel : ObservableChangeNotifyingCollection<ReminderViewModel>
    {
         
    }

    class ReminderViewModel : ChangeNotifier
    {
        public string Title { get { return t; } set { this.SetChangeField(ref t, value); } }
        public ObservableChangeNotifyingCollection<ReminderItemViewModel> Items { get; set; }

        private string t;
        private RemindersViewModel parent;

        public ReminderViewModel(Reminder r, RemindersViewModel parent)
        {
            this.Title = r.Title;
            this.Items = new ObservableChangeNotifyingCollection<ReminderItemViewModel>();
            this.parent = parent;

            r.Content.ForEach(i => this.Items.Add(new ReminderItemViewModel(i, this)));

            this.Items.NewUnsavedChange += () => { this.NewChange(); };
        }

        public void DeleteSelf()
        {
            this.NewChange();
            this.parent.Remove(this);
        }
    }

    class ReminderItemViewModel : ChangeNotifier
    {
        public string Data
        {
            get { return d; }
            set { this.SetChangeField(ref d, value); }
        }

        private string d;

        private ReminderViewModel parent;

        public ReminderItemViewModel(string i, ReminderViewModel parent)
        {
            this.Data = i;
            this.parent = parent;
        }
        public void DeleteSelf()
        {
            this.NewChange();
            this.parent.Items.Remove(this);
        }
    }

    class MdFilesViewModel : ObservableChangeNotifyingCollection<MdFileViewModel>
    {

    }

    class MdFileViewModel : ChangeNotifier
    {
        public string Title { get { return t; } set { this.SetChangeField(ref t, value); } }
        public string Content { get { return c; } set { this.SetChangeField(ref c, value); } }
        public bool EditMode { get { return e; } set { this.SetChangeField(ref e, value); } }

        private string t;
        private string c;
        private bool e;

        public MdFileViewModel(MdFile f)
        {
            this.Title = f.Title;
            this.Content = f.Content;
            this.EditMode = false;
        }
    }

    class ImageViewModel : ChangeNotifier
    {
        public bool Show { get { return s; } set { this.SetChangeField(ref s, value); } }
        public string Image { get { return i; } set { this.SetChangeField(ref i, value); } }

        private bool s;
        private string i;

        public ImageViewModel()
        {
            Show = false;
            Image = placeholder;
        }

        public static string placeholder = "ms-appx:///Images/StoreLogo.png";
    }
}
