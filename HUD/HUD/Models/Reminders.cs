﻿using System.Collections.Generic;

namespace HUD.Models
{
    class Reminders : List<Reminder>
    {
        public Reminders() : base() { }

        public Reminders(IEnumerable<Reminder> reminders)
        {
            foreach (Reminder r in reminders)
                this.Add(r);
        }
    }

    class Reminder
    {
        public string Title { get; set; }
        public List<string> Content { get; set; }
        
        public Reminder(string Title, List<string> Content)
        {
            this.Title = Title;
            this.Content = Content;
        }
    }
}
