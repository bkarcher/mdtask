﻿using System;
using System.Collections.Generic;

namespace HUD.Models
{
    class TodoBoard : List<TodoList>
    {
        public TodoBoard() : base() { }

        public TodoBoard(IEnumerable<TodoList> tl) : base()
        {
            foreach (TodoList l in tl) this.Add(l);
        }
    }

    class TodoList
    {
        public string Title { get; set; }
        public List<Todo> Todos { get; set; }

        public TodoList()
        {
            this.Todos = new List<Todo>();
        }
    }

    class Todo
    {
        public string Title { get; set; }
        public ICollection<string> Info { get; set; }
        public DateTimeOffset? Date { get; set; }

        public Todo()
        {
            this.Info = new List<string>();
        }

        public Todo(string Title)
        {
            this.Title = Title;
            this.Info = new List<string>();
        }

        public Todo(string Title, params string[] Info)
        {
            this.Title = Title;
            this.Info = Info;
        }
    }
}
