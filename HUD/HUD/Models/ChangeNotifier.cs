﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HUD.Models
{
    class ChangeNotifier : INotifyPropertyChanged, IUnsavedNotifier
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event NewUnsavedChangeEventHandler NewUnsavedChange;

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            return true;
        }

        protected void SetChangeField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (this.SetField(ref field, value, propertyName))
                this.NewUnsavedChange?.Invoke();
        }

        protected void NewChange() => this.NewUnsavedChange?.Invoke();
    }
}
