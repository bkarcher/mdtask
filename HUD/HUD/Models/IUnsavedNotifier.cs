﻿namespace HUD.Models
{
    interface IUnsavedNotifier
    {
        event NewUnsavedChangeEventHandler NewUnsavedChange;
    }

    delegate void NewUnsavedChangeEventHandler();
}
