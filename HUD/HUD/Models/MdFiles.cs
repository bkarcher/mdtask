﻿using System.Collections.Generic;

namespace HUD.Models
{
    class MdFiles : List<MdFile>
    {
        public MdFiles() : base() { }

        public MdFiles(IEnumerable<MdFile> files)
        {
            foreach (MdFile f in files)
                this.Add(f);
        }
    }

    class MdFile
    {
        public string Title { get; set; }
        public string Content { get; set; }
        
        public MdFile(string Title, string Content)
        {
            this.Title = Title;
            this.Content = Content;
        }
    }
}
