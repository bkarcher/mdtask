﻿using System;
using System.Collections.Generic;

namespace HUD.Models
{
    class Journal : List<JournalEntry>
    {
        public Journal() : base() { }

        public Journal(IEnumerable<JournalEntry> entries)
        {
            foreach (JournalEntry e in entries)
                this.Add(e);
        }
    }

    class JournalEntry
    {
        public DateTimeOffset Date { get; set; }
        public string Content { get; set; }
        
        public JournalEntry(DateTimeOffset Date, string Content)
        {
            this.Date = Date;
            this.Content = Content;
        }
    }
}
