﻿namespace HUD.Models
{
    class DataModel
    {
        public TodoBoard TodoBoard { get; set; }
        public Journal Journal { get; set; }
        public Reminders Reminders { get; set; }
        public MdFiles MdFiles { get; set; }
        public string Image { get; set; }
    }
}
