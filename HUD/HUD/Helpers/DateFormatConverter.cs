﻿using System;
using Microsoft.UI;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Media;

namespace HUD.Helpers
{
    class DateFormatConverter : IValueConverter
    {
        public DateFormatConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ((DateTimeOffset)value).ToString("yyyy-MM-dd");
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return DateTimeOffset.Parse(value.ToString());
        }
    }

    class DayOfWeekConverter : IValueConverter
    {
        public DayOfWeekConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ((DateTimeOffset)value).ToString("dddd");
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return DateTimeOffset.Parse(value.ToString());
        }
    }

    class DateUrgency : IValueConverter
    {
        public DateUrgency()
        {
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            DateTimeOffset d = (DateTimeOffset)value;

            int daysUntil = d.Subtract(DateTimeOffset.Now).Days;

            if(daysUntil < 0)
            {
                return new SolidColorBrush(Colors.MistyRose);
            }
            if (daysUntil == 0)
            {
                return new SolidColorBrush(Colors.OrangeRed);
            }
            else if(daysUntil < 7)
            {
                return new SolidColorBrush(Colors.Yellow);
            }
            else if(daysUntil < 30)
            {
                return new SolidColorBrush(Colors.SkyBlue);
            }

            return new SolidColorBrush(Colors.Transparent);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return DateTimeOffset.Now;
        }
    }
}