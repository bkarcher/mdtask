﻿using System;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Data;

namespace HUD.Helpers
{
    class PrependBulletConverter : IValueConverter
    {
        public PrependBulletConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return "• " + value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return ((string)value).Remove(0, 2);
        }
    }
}
