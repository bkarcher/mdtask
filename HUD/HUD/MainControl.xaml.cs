﻿using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using System;
using Windows.Storage;
using Windows.Storage.Pickers;
using HUD.Models;
using HUD.Services;
using WinRT;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using Microsoft.UI.Xaml.Media;

namespace HUD
{
    public sealed partial class MainControl : UserControl
    {
        private ViewModel vm;
        private StorageFolder folder;
        private FileStorage storage;

        public MainControl()
        {
            this.InitializeComponent();

            this.vm = new ViewModel();
            this.storage = new FileStorage();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            this.folder = await this.storage.GetStoredPath();

            if (this.folder != null)
                this.vm.Merge(await this.storage.Load(this.folder));
        }

        private async void open_Click(object sender, RoutedEventArgs e)
        {
            FolderPicker fp = new FolderPicker();
            IntPtr windowHandle = (App.Current as App).WindowHandle;
            fp.As<IInitializeWithWindow>().Initialize(windowHandle);
            fp.FileTypeFilter.Add("*");

            StorageFolder f = await fp.PickSingleFolderAsync();

            if (f != null)
            {
                this.folder = f;
                this.vm.Merge(await this.storage.Load(this.folder));
                this.storage.StorePath(folder);
            }
        }

        [ComImport]
        [Guid("3E68D4BD-7135-4D10-8018-9FB6D9F33FA1")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IInitializeWithWindow
        {
            void Initialize(IntPtr hwnd);
        }

        private async void save_Click(object sender, RoutedEventArgs e)
        {
            if (this.folder != null)
            {
                await this.storage.Save(this.folder, this.vm.ToModel());
                this.vm.Unsaved = false;
            }
        }

        private async void refresh_Click(object sender, RoutedEventArgs e)
        {
            if (this.folder != null)
                this.vm.Merge(await this.storage.Load(this.folder));
        }

        private void leftpaneButton_Click(object sender, RoutedEventArgs e)
        {
            this.vm.LeftPaneOpen = !this.vm.LeftPaneOpen;
        }

        private void rightpaneButton_Click(object sender, RoutedEventArgs e)
        {
            this.vm.RightPaneOpen = !this.vm.RightPaneOpen;
        }

        private void DeselectAll(object sender, RoutedEventArgs e)
        {
            foreach(ListView lv in FindVisualChildren<ListView>(this))
            {
                lv.DeselectRange(new Microsoft.UI.Xaml.Data.ItemIndexRange(0, (uint)lv.Items.Count));
            }

            foreach (TodoListViewModel tl in this.vm.TodoBoard)
            {
                foreach (TodoViewModel t in tl.Todos)
                {
                    t.EditMode = false;
                }
            }

            foreach(JournalEntryViewModel j in this.vm.Journal)
            {
                j.EditMode = false;
            }

            foreach (MdFileViewModel m in this.vm.MdFiles)
                m.EditMode = false;
        }

        private void TodoSelect(object sender, SelectionChangedEventArgs e)
        {
            foreach (TodoViewModel t in e.AddedItems)
                t.EditMode = true;

            foreach (TodoViewModel t in e.RemovedItems)
                t.EditMode = false;
        }

        private void InfoRemove(object sender, RoutedEventArgs e)
        {
            (((Button)sender).DataContext as InfoViewModel).DeleteSelf();
        }

        private void AddInfo(object sender, RoutedEventArgs e)
        {
            TodoViewModel t = ((Button)sender).DataContext as TodoViewModel;

            t.Info.Add(new InfoViewModel("", t));
        }

        private void TodoRemove(object sender, RoutedEventArgs e)
        {
            (((Button)sender).DataContext as TodoViewModel).DeleteSelf();
        }

        private void JournalSelect(object sender, SelectionChangedEventArgs e)
        {
            foreach (JournalEntryViewModel j in e.AddedItems)
                j.EditMode = true;

            foreach (JournalEntryViewModel j in e.RemovedItems)
                j.EditMode = false;
        }

        private void AddJournalEntry(object sender, RoutedEventArgs e)
        {
            this.vm.Journal.Add(new JournalEntryViewModel(new JournalEntry(DateTime.Now, "")));
        }

        private void ReminderItemAdd(object sender, RoutedEventArgs e)
        {
            ReminderViewModel r = ((Button)sender).DataContext as ReminderViewModel;

            r.Items.Add(new ReminderItemViewModel("", r));
        }

        private void ReminderItemRemove(object sender, RoutedEventArgs e)
        {
            (((Button)sender).DataContext as ReminderItemViewModel).DeleteSelf();
        }

        private void ReminderRemove(object sender, RoutedEventArgs e)
        {
            (((Button)sender).DataContext as ReminderViewModel).DeleteSelf();
        }

        private void ReminderAdd(object sender, RoutedEventArgs e)
        {
            this.vm.Reminders.Add(new ReminderViewModel(new Reminder("", new List<string>() { }), this.vm.Reminders));
        }

        private void ToggleImage(object sender, RoutedEventArgs e)
        {
            this.vm.Image.Show = !this.vm.Image.Show;
        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
    }
}
